
output "aws_vpc" {
  value = aws_vpc.main.id
}

#output "private_subnets" {
#  value = [ aws_subnet.private_subnets.*.id ]
#}

output "private_subnets" {
  value = "${join(",", aws_subnet.private_subnets.*.id)}"
}

output "public_subnets" {
  value = [ aws_subnet.public_subnets.*.id ]
}