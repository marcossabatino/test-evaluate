resource "aws_instance" "ec2" {
    #instances_per_subnet = var.instances_per_subnet
    ami = var.image
    instance_type = var.instance_type
    key_name = var.key_name
    subnet_id = var.subnet_id
    vpc_security_group_ids = var.security_group
    

    root_block_device {
        volume_size = var.volume_size
    }
}
