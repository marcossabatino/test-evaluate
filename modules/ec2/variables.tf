variable "image" {
  type = string
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "key_name" {
  type = string
}

variable "subnet_id" {
  type = number
}

variable "security_group" {
  type = list
}

variable "volume_size" {
  type = string
}

#variable "instances_per_subnet" {
#  type        = number
#}