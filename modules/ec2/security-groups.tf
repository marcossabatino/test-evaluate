
resource "aws_security_group" "sg_01" {
    name        = "sg_01"
    description = "sg-private-instances security group"
    tags = {
        Name = "sg_01"
    }

    egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

    ingress {
      description      = "port"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = [ "10.0.0.0/16" ]
    }

    ingress {
      description      = "port"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = [ "10.0.0.0/16" ]
    }

    ingress {
      description      = "port"
      from_port        = -1
      to_port          = -1
      protocol         = "icmp"
      cidr_blocks      = [ "0.0.0.0/0" ]
    }
}

