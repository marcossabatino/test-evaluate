module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name   = "test-evaluate"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3FthKVVkwC8NW+vakPe8XNwoJWxiqunUlQomoxkHe31KVTHyDX/jC/orMNGFGwCCQXQY3c3U8EvcP5vPftmOGtHyu/SI4yvTLZv6reCcOS/HeGLDu7VflR0QGg/utcFV0nOcITULq8X02OSd8cnkX5ZY31+H5YY2yWr7e9Gnv5JmpK9EtIA0nNF3fJUnMawEQs5Yc1luw8/92bTwAK92I+WF0gLKquN9QVRDl7+EzF+Opas31JlK4AatJSFzf5S5OX2kHSGaV5VF0rCWltv06DGZ4CSobtJ7zypAQReZ3BY0AD8HfXAXqyZIMavHpgtJ41ZgaypidCC3WDOEx0wqnIhMFVrmELZYogzyqnluVuDMzF30J0GGAUnrSm9ZcFMS64SdPZgl46aqwHD8Uq2J1uo4Tjlp1+XxtfxsgCKAvA0K0GV+rybeOHJS+rbcWujtc6KnrP25p/PJ/oAkpi6Txi/+2Ff/0OzvhogTz6zpbsj+GgpteoBnt0ULaPK6UaGM="
}


module "ec2" {
    source = "./modules/ec2"
    count = 4
    image = "ami-09a41e26df464c548"
    #instance_type = "t2.micro"
    key_name = "test-evaluate"
    subnet_id = "${element(split(",", module.vpc.private_subnets), count.index)}"
    security_group = [ "sg-01" ]
    volume_size = "10"
    depends_on = [
      module.key_pair,
      module.vpc
    ]
}

module "vpc" {
    source = "./modules/vpc"
    azs = ["us-east-1a", "us-east-1b", "us-east-1c"]
    private_subnet_cidrs = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
    public_subnet_cidrs = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

resource "aws_eip" "eip_ip" {
  vpc              = true
  public_ipv4_pool = "amazon"
}

resource "aws_nat_gateway" "nat_gw_01" {
    depends_on=[aws_eip.eip_ip]
  allocation_id = aws_eip.eip_ip.id
  subnet_id = module.vpc.aws_vpc
tags = {
    Name = "nat_gw_01"
  }
}

resource "aws_route_table" "private_subnet_route_table" {
      depends_on=[aws_nat_gateway.nat_gw_01]
  vpc_id = module.vpc.aws_vpc

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw_01.id
  }

  tags = {
    Name = "private_subnet_route_table"
  }
}

resource "aws_route_table_association" "private_subnet_route_table_association" {
  depends_on = [aws_route_table.private_subnet_route_table]
  subnet_id      = module.vpc.aws_vpc
  route_table_id = aws_route_table.private_subnet_route_table.id
}

resource "aws_instance" "BASTION" {
  ami           = "ami-0732b62d310b80e97"
  instance_type = "t2.micro"
  subnet_id = module.vpc.aws_vpc
  key_name = "test-evaluate"
  associate_public_ip_address = true

  tags = {
    Name = "bastionhost"
    }
}

variable "vpc_id" {
  type = string
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public Subnet CIDR values"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

resource "aws_security_group" "alb" {
  name        = "terraform_alb_security_group"
  description = "Terraform load balancer security group"
  vpc_id = data.aws_vpc.selected.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.public_subnet_cidrs
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
